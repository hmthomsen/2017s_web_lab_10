-- Answers to Exercise 6 here

-- Add the DROP TABLE IF EXISTS while in development to get rid of all existing tables
DROP TABLE IF EXISTS dbtest_VideoStore_CustomerInfo;

-- Creates the customers table if it doesn't exist
CREATE TABLE IF NOT EXISTS `dbtest_VideoStore_CustomerInfo`
(
  id INT NOT NULL AUTO_INCREMENT,
   name VARCHAR(32),
  gender VARCHAR(16),
  year_born INT,
  year_joined INT,
  num_hires INT,
  PRIMARY KEY (id)
);

-- Creates the vids table if it doesn't exist
CREATE TABLE IF NOT EXISTS `dbtest_VideoStore_VidsInfo`
(
  barcode INT NOT NULL AUTO_INCREMENT,
  vid_title VARCHAR(32),
  vid_director VARCHAR(32),
  vid_rate INT(4),
  current_renter INT,
  PRIMARY KEY (barcode)
);

ALTER TABLE dbtest_VideoStore_VidsInfo
  ADD FOREIGN KEY (current_renter) REFERENCES dbtest_VideoStore_CustomerInfo (id);

INSERT INTO dbtest_VideoStore_VidsInfo (vid_title, vid_director, vid_rate, current_renter)  VALUES
  ("Titanic", "Jane Director", "4", "1"),
  ("StarWars", "Her", "3", "1"),
  ("Empire Strikes", "Logar", "6", "1"),
  ("Jedi", "Jane Director", "2", "1");


INSERT INTO dbtest_VideoStore_CustomerInfo (name, gender, year_born, year_joined, num_hires) VALUES
  ("Jane Campion", "female", "1954", "1980", 30000),
  ("Roger Donaldson", "male", "1945", "1980", 12000),
  ("Temuera Morrison", "male", "1960", "1995", 15500),
  ("Russell Crowe", "male", "1964", "1990", 10000),
  ("Lucy Lawless", "female", "1968", "1995", 5000),
  ("Michael Hurst", "male", "1957", "2000", 15000),
  ("Andrew Niccol", "male", "1964", "1997", 3500),
  ("Kiri Te Kanawa", "female", "1944", "1997", 500),
  ("Lorde", "female", "1996", "2010", 1000),
  ("Scribe", "male", "1979", "2000", 5000),
  ("Kimbra", "female", "1990", "2005", 7000);

SELECT * FROM dbtest_VideoStore_VidsInfo;
SELECT * FROM dbtest_VideoStore_CustomerInfo;
