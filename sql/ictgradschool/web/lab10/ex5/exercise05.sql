-- Answers to Exercise -- Answers to Exercise 2 here
-- Add the DROP TABLE IF EXISTS while in development to get rid of all existing tables
DROP TABLE IF EXISTS dbtest_Exercise5_Users;

-- Creates the table if it doesn't exist
CREATE TABLE IF NOT EXISTS dbtest_Exercise5_Users (
  id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(16),
  first_name VARCHAR(32),
  last_name VARCHAR(32),
  email VARCHAR(32),
  PRIMARY KEY (id, username)
);

-- Inserts values into the table
 INSERT INTO dbtest_Exercise5_Users (username, first_name, last_name, email) VALUES
 ('test_thomsen', 'test', 'thomsen', 'test@gmail.com'),
('test_thomsen', 'abc', 'trn', 'R@gmail.com'),
 ('peterwithanr', 'Peter', 'Jones', 'Peter@gmail.com'),
 ('PeterwithoutanR', 'Pete', 'Joners', 'Peter@yahoo.com'),
 ('PetersonIsMylast', 'Jimmy', 'Peterson', 'Jimmy@Peterson.com'),
 ('billgates', 'Bill', 'Gates', 'bill@Microsoft.com')

SELECT * FROM dbtest_Exercise5_Users
