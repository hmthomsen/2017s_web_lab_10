-- Answers to Exercise 9 here
-- All info
SELECT id, name, gender, year_born, year_joined,num_hires FROM `dbtest_VideoStore_CustomerInfo`

-- All except number of hires
SELECT id, name, gender, year_born, year_joined FROM `dbtest_VideoStore_CustomerInfo`

-- Vid Directors
SELECT * FROM dbtest_VideoStore_VidsInfo