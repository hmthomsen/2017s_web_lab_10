-- Answers to Exercise 8 are at the bottom
-- 1st Drop the Table, Create it and then populate it

-- Add the DROP TABLE IF EXISTS while in development to get rid of all existing tables
DROP TABLE IF EXISTS dbtest_Exercise8_Content_Articles;


-- Creates the table if it doesn't exist-- Creates the table if it doesn't exist
CREATE TABLE IF NOT EXISTS dbtest_Exercise8_Content_Articles
(
  id INT NOT NULL AUTO_INCREMENT,
  publicationDate DATE,
  article_title VARCHAR(64),
  article_summary TEXT,
  article_content MEDIUMTEXT,
  page_title TEXT,
  PRIMARY KEY (id)
);

-- Insert 1 article including HTML of article
INSERT INTO dbtest_Exercise8_Content_Articles (publicationDate, article_title, article_summary, article_content, page_title) VALUES
  ('2017-12-24', '2nd Test Article', 'Lorem ipsum dolor sit amet consectetur adipiscing elit, torquent volutpat phasellus maecenas dis ad, quis quam ridiculus felis primis tincidunt. Per semper sociis netus himenaeos congue metus eleifend diam sed sociosqu montes massa eget','<p>Lorem ipsum was conceived as filler text, formatted in a certain way to enable the presentation of graphic elements in documents, without the need for formal copy. Using Lorem Ipsum allows designers to put together layouts and the form of the content before the content has been created, giving the design and production process more freedom.</p><p>It is widely believed that the history of Lorem Ipsum originates with <a href="https://en.wikipedia.org/wiki/Cicero">Cicero</a> in the 1st Century BC and his text <i>De Finibus bonorum et malorum</i>. This philosophical work, also known as <i>On the Ends of Good and Evil</i>, was split into five books. The Lorem Ipsum we know today is derived from parts of the first book <i>Liber Primus</i> and its discussion on hedonism, the words of which had been altered, added and removed to make it nonsensical and improper Latin. It is not known exactly when the text gained its current traditional form. However references to the phrase "lorem ipsum" can be found in the <i>1914 Loeb Classical Library Edition of the De Finibus</i> in sections 32 and 33.</p>	<p>It was in this edition of the De Finibus that H. Rackman translated the text. The following excerpt is taken from section 32:</p><blockquote><p>"qui dolorem ipsum, quia dolor sit amet consectetur adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem".</p></blockquote>
<p>This is recognisable, in part, as today''s standard Lorem Ipsum and was translated into:</p>','Lorem Ipsum Page Title' )

DELETE FROM dbtest_Exercise8_Content_Articles WHERE page_title ='Lorem Ipsum Page Title';
-- Ex 8
CREATE TABLE IF NOT EXISTS dbtest_Exercise8_Content_Articles
(
  id INT NOT NULL AUTO_INCREMENT,
  publicationDate DATE,
  article_title VARCHAR(64),
  article_summary TEXT,
  article_content MEDIUMTEXT,
  page_title TEXT,
  PRIMARY KEY (id)
);

-- Insert 1 article including HTML of article
INSERT INTO dbtest_Exercise8_Content_Articles (publicationDate, article_title, article_summary, article_content, page_title) VALUES
  ('2017-12-24', '2nd Test Article', 'Lorem ipsum dolor sit amet consectetur adipiscing elit, torquent volutpat phasellus maecenas dis ad, quis quam ridiculus felis primis tincidunt. Per semper sociis netus himenaeos congue metus eleifend diam sed sociosqu montes massa eget','<p>Lorem ipsum was conceived as filler text, formatted in a certain way to enable the presentation of graphic elements in documents, without the need for formal copy. Using Lorem Ipsum allows designers to put together layouts and the form of the content before the content has been created, giving the design and production process more freedom.</p><p>It is widely believed that the history of Lorem Ipsum originates with <a href="https://en.wikipedia.org/wiki/Cicero">Cicero</a> in the 1st Century BC and his text <i>De Finibus bonorum et malorum</i>. This philosophical work, also known as <i>On the Ends of Good and Evil</i>, was split into five books. The Lorem Ipsum we know today is derived from parts of the first book <i>Liber Primus</i> and its discussion on hedonism, the words of which had been altered, added and removed to make it nonsensical and improper Latin. It is not known exactly when the text gained its current traditional form. However references to the phrase "lorem ipsum" can be found in the <i>1914 Loeb Classical Library Edition of the De Finibus</i> in sections 32 and 33.</p>	<p>It was in this edition of the De Finibus that H. Rackman translated the text. The following excerpt is taken from section 32:</p><blockquote><p>"qui dolorem ipsum, quia dolor sit amet consectetur adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem".</p></blockquote>
<p>This is recognisable, in part, as today''s standard Lorem Ipsum and was translated into:</p>','Lorem Ipsum Page Title' )
